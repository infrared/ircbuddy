package Ircbuddy::Modules::Dispatch;

use strict;
use warnings;
use Module::Loaded;


my %dispatch = (
    
    ping => {
		match => qr/^ping$/,
        module => "Ircbuddy::Modules::Ping",
		method => "go",
    },
	calc => {
		match => qr/^calc/,
		module => "Ircbuddy::Modules::Calc",
		method => "go",
	},
	sup => {
		match => qr/sup\?|yo|hey|hi|my nigga/i,
		module => "Ircbuddy::Modules::Sup",
		method => "sup",
	},
	reload => {
		match => qr/reload\s+(\S+)/i,
		module => "Ircbuddy::Modules::Reload",
		method => "reload",
		},

);

sub dispatch {
    my ($self,$bot,$mess,$schema) = @_;
		
	for my $key (keys %dispatch) {
		

		if ($mess->{'body'} =~ $dispatch{$key}{'match'}) {
			
		#	print STDERR "[debug] " . $key . " : " . $dispatch{$key}{'match'} . "\n";
			
        	my $module = $dispatch{$key}{'module'};
			my $method = $dispatch{$key}{'method'};
        	
			if (is_loaded($module)) {

            	if (exists $dispatch{$key}{auth}) {
                	# If we're here, then the module requires the
                	# user to have a certain role to use it.

                	my $who = $mess->{who};
                	my $raw = $mess->{raw_nick};

                	# Check if user exists in the database
                	my $search = $schema->resultset('Users')->search({ username => $who });
                
                	if ($search->count) {
                    	my $user = $search->first;
                  
                  
                    	# If raw_nick in the database matches the user's raw (ident@host)
                    	# then the user is authenticated
                    	if ($user->raw_nick eq $raw) {
                      
                      
                        	# Get the user role, and see if it matches
                        	# the module's requirement
                        	my $role = $user->role;
                        	if (grep($_ eq $role,  @{ $dispatch{$key}{auth} })) {
                            
                            # yay! we got this far, the user: exists, is authenticated, and
                            # has the correct role
                        
  
                            	if (exists $dispatch{$key}{'where'}) {
                            		# If were are here, the module requires the user request to
                           		 	# be chatted in a specific way; channel or private message
                                
                           			if (
                           	 			($mess->{channel} eq 'msg' && $dispatch{$key}{where} eq 'private') ||
                          				($mess->{channel} ne 'msg' && $dispatch{$key}{where} eq 'channel')) 
										{
     
                                    		eval{ $module->$method($bot,$mess,$schema) };
                                    	    $bot->reply($mess,$@) if $@; # Exception, reply with error
                                	}
                                	else {
                                    	$bot->reply($mess,"That function is limited to ".$dispatch{$key}{where}. " message");
                                	}
                           	   	}
                            	else {
                                	# 'where' is not specified by the module,
                                	# so just do it

                                	eval{ $module->$method($bot,$mess,$schema) };
                                	$bot->reply($mess,$@) if $@; # Exception, reply with error
                            	}
                        	}
                        	else {
                            	# User exists, is authenticated, but does not
                            	# have the correct role
                            	$bot->reply($mess,"Insufficient privileges");
                        	}
                    	}
                    	else {
                        	# The user exists, but is not authenticated,
                        	# raw_nick does not match user<ident@hostname>
                        	$bot->reply($mess,"That request requires authorization");
                        
                    	}
                	}
                	else {
                    	# user is not in database at all
                    	$bot->reply($mess,"That request requires registration and privileges");
                    
                	}
            	}
            	else {
                	# The module is wide open. Anyone can use this module! fun fun!
                	if (exists $dispatch{$key}{'where'}) {
                    
                    #
                   
                    	if (
                        	($mess->{channel} eq 'msg' && $dispatch{$key}{where} eq 'private') ||
                        	($mess->{channel} ne 'msg' && $dispatch{$key}{where} eq 'channel')) 
							{
     
                        		eval{ $module->$method($bot,$mess,$schema) };
                        		$bot->reply($mess,$@) if $@; # Exception, reply with error
                    	}
                    	else {
                        	$bot->reply($mess,"That function is limited to ".$dispatch{$key}{where}. " message");
                    	}
                	}
                	else {
                    	eval{ $module->$method($bot,$mess,$schema) };
                    	$bot->reply($mess,$@) if $@; # Exception, reply with error
                	}
               
            	}
        	}
        	else {
            	$bot->reply($mess,"that module is not loaded");
        	}
			last;
    	}
    
	}
    
}
1;
