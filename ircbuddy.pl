package Ircbuddy;


use Module::Loaded;
use Module::Find;
use FindBin::Real;
use lib FindBin::Real::Bin() .'/lib';
use Ircbuddy::Schema::Main;


my $ai;

my $schema = Ircbuddy::Schema::Main->connect('dbi:SQLite:db/ircbuddy.db') or die $!;

#Load all our modules
my @modules  =  useall Ircbuddy::Modules;
print "Loaded $_\n" for @modules;



use base qw/Bot::BasicBot/;

my $bot = Ircbuddy->new(

	server => "irc.freenode.net",
    port   => "6667",
    channels => ["#ircbuddy"],
	nick => "boogerbot__",
    alt_nicks => ["boogerbot_", "boogerbot__"],
	
 #   nick      => "ircbuddy" . ( int( rand( 999)) + 123),
    username  => "ircbuddy",
    name      => "ircbuddy",

#    ignore_list => [qw(Chanserv)];
);

sub said {
	my ($self,$mess) = @_;
  

	if (exists $mess->{address}) {
	    print STDERR "[" . scalar localtime . "] " .$mess->{'channel'} . " " .$mess->{who} ." : ".$mess->{body}."\n";


		if (is_loaded("Ircbuddy::Modules::Dispatch")) {
		
			eval { Ircbuddy::Modules::Dispatch->dispatch($self,$mess,$schema) };
			$self->reply($mess,$@) if $@;  # Exception, send error message to channel
	  	}
		else {
			print STDERR "Dispatch module not loaded\n"
		}	
	}
}

sub help {
	my ($self,$mess) = @_;

	if (is_loaded("Ircbuddy::Modules::Help")) {
		eval { Ircbuddy::Modules::Help->go($self,$mess) };
		$self->reply($mess,$@) if $@;
	}
	else {
		$self->reply($mess,"I can't help you");
	}
}


$bot->run();
